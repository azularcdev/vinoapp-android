import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import "../src/config/Reactotron"

Enzyme.configure({ adapter: new Adapter() })

Date.now = jest.fn(() => 22)

jest.mock('services/utils/ConvertTimestamp', () => ({
  convertTimestamp: (date) => {
    console.log('jest mock convertTimestamp')
    return 'A year ago'
  }
}))

const moment = require('moment-timezone')
moment.tz.setDefault("America/New_York");

jest.mock('react-native-navigation', () => ({
  Navigation: {
    startNavigation  : jest.fn(),
    registerComponent: jest.fn(),
    startTabBasedApp : jest.fn(),
    startSingleScreenApp: jest.fn(),
  }
}))

jest.mock("AsyncStorage", () => {
  const items = []
  return {
    setItem: (item, value) => {
      return new Promise((resolve, reject) => {
        items[item] = value
        resolve(value)
      })
    },
    multiSet: (item, value) => {
      return new Promise((resolve, reject) => {
        items[item] = value
        resolve(value)
      })
    },
    multiSet: (item, value) => {
      return new Promise((resolve, reject) => {
        items[item] = value
        resolve(value)
      })
    },
    getItem: (item, value) => {
      return new Promise((resolve, reject) => {
        resolve(items[item])
      })
    },
    multiGet: jest.fn(item => {
      return new Promise((resolve, reject) => {
        resolve(items[item])
      })
    }),
    removeItem: jest.fn(item => {
      return new Promise((resolve, reject) => {
        resolve(delete items[item])
      })
    }),
    getAllKeys: jest.fn(items => {
      return new Promise(resolve => {
        resolve(items.keys())
      })
    })
  }
})
