import reactotron from 'config/Reactotron'
import i18n from 'lang'
import I18n from 'react-native-i18n'
import { Text } from 'react-native'
import { Provider } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import screens, { SCREEN_IDS, registerScreens, } from './screens'
import configureStore from './store'

const store = configureStore()
registerScreens(store, Provider)

Navigation.startSingleScreenApp({
  screen: {
    screen: SCREEN_IDS.ROOT,
    navigatorStyle: {
      navBarHidden: true,
    },
  },
  appStyle: {
    orientation: 'portrait'
  },
  animationType: 'fade'
})
