# VinoApp (v3 refactor)
Multi platform mobile app. 
The project main libraries are:
- react-native-navigation
- react-native-firebase
- 

# Getting Started

## Table of Contents
1. [Git Structure](#gitstructure)
1. [Requirements](#requirements)
1. [Installation](#getting-started)
1. [Running the Project](#running-the-project)
1. [Architecture](#architecture)
    * [Project Structure](#project-structure)
    * [Core Arch](#core-arch)
        * [Modules](#modules)
        * [Redux](#redux)
        * [Sagas](#sagas)
        * [Immutability](#Immutability)
        * [ReduxPersist](#persist)
1. [Development](#local-development)
    * [Alias & Env Config](#alias)
    * [ESlint](#eslint)
    * [Reactotron](#reactotron)
	  * [Visual Assets](#visual-assets)
1. [Routing](#routing)
1. [Testing](#testing)
    * [Jest](#jest)
    * [Enzyme](#Enzyme)
1. [Building for Production](#building-for-production)
    * [Android](#android)
    * [IOS](#ios)

## Git Structure

Our git branching structure is similar to the [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow) workflow.

The Master branch stores the official release history and is tagged with release versions. The 'dev' branch
serves as the integration branch for features.

Features should be developed in a dedicated branch branched from dev. When a feature branch is ready to be integrated, a Pull Request
(Merge Requests in gitlab) to facilitate code review is made. A designated reviewer will merge in the branch once a review has been completed.

## Requirements

* node `^8.0.0`
* yarn `^1.2.1` or npm `^5.5.1`
* CocoaPods `^1.5.0`
* react-native-cli `^2.0.0`
* react-native `^0.50.0`
* reactotron `^1.11.0`(optional)

## Installation

* Install the project dependencies. It is recommended that you use [Yarn](https://yarnpkg.com/) for deterministic dependency management, but `npm install` will suffice.
```bash
$ yarn  # Install project dependencies (or `npm install`)
```

For ios case, its require to install the pods. 
```bash
$ cd ios  
$ pod install  
```

## Running the Project

You need to copy (the existing **.env.example** file) or use your own **.env** file in the root folder of the project.
After that you can run the project with:

```bash
$ react-native run ios  # Run the project on Ios
```

or

```bash
$ react-native run android  # Run the project on Android
```


## Project commands

|`yarn run <script>`|Description|
|------------------|-----------|
|`start`|Start the react native packager on port 8081.|
|`ios`|Start the packager, a IOS simulator and run the application on IOS.|
|`android`|Start the packager and run the application on Android. A device or emulator running is required|
|`test`|Runs unit tests with Jest.|
|`test:watch`|Runs Jest and watches for changes to re-run tests; does not generate coverage reports.|
|`test:coverage`|Runs unit test with Jest and generate coverage reports in the coverage folder.|
|`test:snapshot`|Runs unit test with Jest and re generate the current snapshots.|
|`lint`|Run basic linting|
|`lint:fix`|Run linting with --fix|

## Architecture

### Application Structure

The application structure is **fractal**, where functionality is grouped primarily by feature rather than file type. This structure is only meant to serve as a guide.

```
.
├── ios                      # IOS Native Project Files (Xcode)
├── android                  # Android Native Project Files (Android Studio)
├── index.js                 # React Native aplication entry point.
├── src                      # Application source code
│   ├── app.js               # Application bootstrap and rendering
│   ├── components           # Global Reusable Presentational Components
│   ├── config               # Global config for plugins and external modules
│   ├── images               # Visual assets
│   ├── modules              # Global modules (transversal reduxs and sagas)
│   │   ├── reducers.js      # Global reducer combination and utils for on demand injection.
│   │   ├── sagas.js         # Global sagas running and utils for on demand injection.
│   │   └── startup          # Module (redux/saga/actions) to work with the application startup
│   │       ├── actions.js   # Actions Type and Actions Creators for startup module
│   │       ├── reducer.js   # Reducer for startup module
│   │       └── saga.js      # Sagas for startup module
│   ├── screens              # Main route definitions and async split points
│   │   ├── index.js         # Bootstrap main application routes with store
│   │   └── Example          # Fractal route
│   │       ├── index.js     # Example route definition
│   │       ├── assets       #
│   │       ├── containers   # Connect components to actions and store
│   │       └── component    #
│   ├── services             #
│   ├── store                # Redux-specific pieces
│   │   ├── createStore.js   # Create and instrument redux store
│   │   └── index.js         # Expose the creation the store with the saga addition.
│   └── theme                # Global styles and measures
├── __tests__                # Globals unitest files
└── jest                     # Jest helpers, utils, and unitest coverage
```

### Core Arch
- (TODO)

## Development

### Alias & Config
For the imports of modules in the project you can import all from relative path to `/src/` folder.
For example if you want to import a component inside `src/components/MyComponent`, you don't need to specify a relative path, the import would look like this: `import MyComponent from components/MyComponent`
We recomend using only relative paths if the diference of level between folders its 1 or 2.

The project uses dot env files for global configuration variables, this mean that you can access the global configuration vars for the current env doing: `import Config from react-native-config`. And all var will be available on that object.

### ESlint
The eslint configuration can be founded in the .eslintrc. This extends the base configuration of [eslint-config-airbnb](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb) and add some custom rules.

### Reactotron

**The project uses [Reactotron](https://github.com/infinitered/reactotron).**
Using this allows your monitors to run on a separate thread and affords better performance and functionality. It comes with several of the most popular monitors, is easy to configure and filters actions.  The configuration of important flags and exceptions can be found ad `src/config/Reactotron`.

### Visual Assets
All assets must have at least 2 different sizes (correctly scaled and named) to cover the largest number of devices with different pixel densities.
The file's name convention is :
* filename.jpg
* filename@2x.jpg
* filename@3x.jpg


## Routing
- [react-native-navigation](https://github.com/wix/react-native-navigation)

### Connecting Native Routing Events to Redux with HOC

* src/hocs/ScreenHoc
* Dispatches a redux action informing when the native navigation library is transitioning to a new screen
* Provides methods for wrapped screens to "subscribe" to: onNavigationEvent

** A screen view may define these methods and will receive a nav event object as input**
* onNavigationEvent provides all navigation events from react-native-navigation
** Note: return true from a screen's onNavigationEvent to stop event propagation for the "back" event if you wish to handle custom back events**

TODO - full function definitions

## Testing
- TODO

### Jest
### Enzyme

## Building for Production
With te correct credentials/keys/profiles excecute :

### IOS
- Remember update the version before build.
From xcode ...

### Android
- Remember update the version number and ANDROID_BUILD_NUMBER, APP_VERSION_CODE & APP_VERSION_NAME on the .env file before build.
- The keystore are included on the project. The password and alias can be found on the gradle.properties file.

```bash
$ cd android
$ ./gradlew assembleRelease
```
